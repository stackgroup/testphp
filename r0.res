resource r0 {
    protocol C;

    startup {
        #become-primary-on both;
        wfc-timeout  15;
        degr-wfc-timeout 60;
    }
    net {
    #allow-two-primaries yes;
        cram-hmac-alg sha1;
        shared-secret "cmschina_drbd";
    }
    disk {
        resync-rate 20M;

    }
    handlers {
        # split-brain "/xxx/sb.sh";
    }

    on primary {
        device /dev/drbd0;
        disk /dev/sdb1;
        address 192.168.2.135:7788;
        meta-disk internal;
    }
    on backup {
        device /dev/drbd0;
        disk /dev/sdb1;
        address 192.168.2.136:7788;
        meta-disk internal;
    }
}